export const initialState = {
  configEndpoint: '',
  reportId: null,
  groupName: null,
  groupPath: null,
  config: {
    title: 'Report',
  },
};

export const pageData = {
  configEndpoint: 'foo_bar_endpoint',
  reportId: 'foo_bar_id',
  groupName: 'Foo Bar',
  groupPath: 'foo_bar',
};

export const configData = {
  title: 'Foo Bar Report',
};
